# WinForms (GUI) style app prototype, on Windows, in Small Visual Basic

## Version

v0.1.0

## About

Visual Small Basic prototype app written in [**Small Visual Basic (sVB)**](https://marketplace.visualstudio.com/items?itemName=ModernVBNET.sVBInstaller), a derivative of [**Microsoft's Small Basic**](http://smallbasic.com) language.

![sVBApp1.png](./sVBApp1.png?raw=true)

## History

0.1.2:
-fix trap of New filename embedded in path of filename to be saved
-fixed If/Else structure during Save

0.1.1:
-post fixed call by Edit|Preferences to File.OpenFolderDialog()
-make Status and Error fields clickable to show a messagebox containing the full message
-change background on status bar fields to indicate field locations

0.1.0:
-Initial release

Steve Sepan
sjsepan@yahoo.com
5-27-2023
